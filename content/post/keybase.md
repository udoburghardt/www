---
Title: Keybase
Date: 2018-04-25
draft: false
Category: Review
Tags: [crypto, email, sicherheit, verschlüsselung]
description: keybase.io eliminiert einige Hürden, die bisher die Verbreitung von PGP/OpenPGP behindert haben. Dazu wird sowohl Infrastruktur als auch kostenfreie Software zur Verfügung gestellt. Zusätzlich gibt es Goodies wie Zugriff auf Dateien per Filesystem-Treiber
---

Email-Verschlüsselung genießt seit Jahrzehnten nur sehr langsame Verbreitung. Mit *KeyBase* versucht eine weitere Firma, die Einstiegshürde zu senken.


Highlights
----------
* das "KBFS" (**K**ey**B**ase-**F**ile**S**ystem) stellt sowohl unter
Linux wie auch unter Windows ein virtuelles Laufwerk zur Verfügung.
Die dort abgespeicherten Dateien sind mit dem eigenen OpenPGP-Key
verschlüsselt, und können dennoch mit Freunden geteilt werden. 
Das funktioniert prima ist mein persönliches TOP Feature. 
* die Verifizierbarkeit einer Identität wird duch "Beweise" ("Proof") erreicht.
Wenn man jemanden über Facebook, Twitter, Github, Reddit oder einige andere 
Dienste kennt, kann man hier sicherstellen, dass dies dieselbe Person ist. 
* Ver- und Entschlüsselung von beliebigem Text ist im Brauserfenster möglich.
* git-Repositories als integraler Bestandteil
* Chat - zwischen Personen und auch in Gruppen

Nachteile
---------
* ?


Links
-----

* <https://keybase.io/udoburghardt>  --  meine Einstiegseite


