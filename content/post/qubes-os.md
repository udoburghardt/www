---
Title: Qubes-OS
Date: 2018-04-25
Category: Review
Tags: [crypto, sicherheit, verschlüsselung, os, linux, xen] 
Description: Qubes-OS liefert einen -im Vergleich- *sicheren* Linux-Desktop mit Hilfe von Xen und virtueller Maschinen. Dazu werden Zonen mit unterschiedlichen Vertrauens-Status aufgebaut, in denen dann die üblichen Anwendungen laufen 
---

Dass man mit virtuellen Maschinen einzelne Aufgaben voneinander trennen kann ist klar. 
Bisher fehlte aber ein Mechanismus, dies mit einem sicheren Unterbau zu machen. 
Es nützt nämlich nichts, wenn man mit 23 virtuellen Maschinen in VirtualBox arbeitet 
und unten drunter läuft ein unsicheres Windows.  

Mit Qubes-OS werden Programme in Zonen platziert, die jeweils voneinander isoliert sind. 
Untendrunter liegt hier ein XEN/KVM-System, welches ausschliesslich der Verwaltung dient 
und (beispielsweise) nicht auf das Internet zugreifen darf. 
Jegliche Nutzung geschieht innerhalb einer virtuellen Maschine.


Highlights
----------
* freie Sofware 
* Xen
* interessante Details wie "Wegwerf"-VMs mit (beispielsweise) einem Webbrauser


Nachteile
---------
* der verwendete Rechner mus "neu" sein, da die CPU-internen Virtualisierungsfunktionen genutzt werden
* durch die zusätzliche Sicherheit wird die Handhabung unbequemer
* die Einrichtung bedarfsgerecht aufgeteilter Software in spezifischen "Domains" ist nicht wirklich trivial und bedingt Einarbeitung, um das Konzept zu verstehen

Links
-----
* https://www.qubes-os.org
* https://www.qubes-os.org/doc/system-requirements/

