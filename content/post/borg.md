---
Title: Borg
Date: 2018-07-15
draft: false
Category: Review
Tags: [backup, crypto, sicherheit, verschlüsselung]
description: Borg Backup erstellt Sicherungskopien und wendet beim Abspeichern der zu sichernden Daten automatisch Deduplizierung an. Ausserdem sind die Backups verschlüsselt und komprimiert.
---

Im einfachsten Fall erstellt man ein Backup, indem man einfach alle zu sichernden Dateien kopiert. Das ginge natürlich, aber in diesem Fall würde (beispielsweise) täglich das gesamte zu sichernde Volumen auf das Zielmedium zu schreiben sein. 

Mit Borg werden die zu sichernden Daten stattdessen zunächst in kleinere Blöcke (mit dynamischen, fließenden Grenzen) zerlegt und dieser Block dann nur tatsächlich abgespeichert, sofern nicht bereits früher ein identischer Block gesichert wurde. Dieses Konzept nennt man Deduplizierung. Der gewünschte Effekt ist klar: sich *wiederholende* Sicherungen werden kleiner. (Bei nur einem einzigen Backup wäre/ist dieser Effekt gleich Null.) Der Umfang, in dem das tatsächlich geschieht, ist allerdings verblüffend. In meinem konkreten Anwendungsfall - ich sichere damit Backups virtueller Maschinen - befinden sich in dem Repository 10 Backups. Jedes *einzelne* Backup ist knapp 1 TB groß, es werden pro Tag also fast 1 TB gesichert. Alle Sicherungen *zusammen* belegen aber in meinem Fall nur 1.6 TB - statt 10 TB - auf der Festplatte. Das finde ich wirklich verblüffend!



Highlights
----------
* Deduplizierung
* Verschlüsselung
* Komprimierung
* kann via SSH arbeiten - damit sind "Remote Repositories" problemlos möglich

Nachteile
---------
* keine


Links
-----

* <https://www.borgbackup.org>  --  Projektseite
* <https://borgbackup.readthedocs.io/en/stable/>  --  Dokumentation



