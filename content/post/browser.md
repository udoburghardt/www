---
Title: Browser
Date: 2020-04-04
draft: true
Category: Review
Tags: [browser, crypto, sicherheit, privacy]
description: Browser als wichtigstes Interface zum "www".
---

Für viele Menschen besteht das Internet ausschließlich aus dem WWW. Daraus folgt, dass der normale Webbrowser "das Internet ist".




Tatsächlcih verwende ich diverse Browser in etlichen Instanzen *jeden Tag*. 


Firefox
-------
* ... ist zwar nicht immer die erste Wahl, aber dennoch häufig der Default. In *jeder* verwendeten Instanz ist mindestens uBlock Origin installiert

Iridium
-------
* Zitat: <cite>"A browser securing your privacy. That’s it."</cite>
* Dies ist meine Wahl zum jetzigen Zeitpunkt (2019/2020)

* https://iridiumbrowser.de/  --  deutscher Hersteller
* https://github.com/iridium-browser/tracker/wiki/Differences-between-Iridium-and-Chromium -- detailreich!

Brave
-----
* Zitat: <cite>Get unmatched speed, security and privacy by blocking trackers</cite>
* der Hersteller möchte, dass man "Brave Rewards" verdient - durch Konsumierung von Werbung. Das erscheint kontraproduktiv, ist aber anscheinend sauber implementiert


* https://brave.com/
* https://spyware.neocities.org/articles/browsers.html  --  Gegenmeinung <cite>"Low Tier - Poor Privacy"</cite>


Vivaldi
-------
* Zitat: <cite>We do our utmost to protect the security of personal information for users of Vivaldi.<br />Da stand *früher* mal: <cite>"We don't track you - Keep your private data private"</cite>
* https://vivaldi.com



Chromium
--------
* beinhaltet angeblich auch in der "nackten" Version immer noch Google-Tracking und ungünstige Default-Einstellungen

Iron
----
* https://www.srware.net/software_srware_iron_chrome_vs_iron.php
* mangelhafte Privacy laut https://spyware.neocities.org/articles/browsers.html "Shit tier - No Privacy", auch in 2019



Ungoogled Chromium
------------------
* ...



Dissenter
---------
* ????
* https://dissenter.com/




Highlights
----------
* reduzierte Verfolgung der eigenen Netznutzung

Nachteile
---------
* keine


Links
-----
* ...



