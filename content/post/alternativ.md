---
Title: Alternativen *sind* verfügbar!
Date: 2018-07-05
draft: false
Category: Review
Tags: [software, self-hosted, frei, privacy]
description: Oft liegt die Begründung für die Nutzung "üblicher" Software schlicht in der Unkenntnis der vorhandenen Alternativen. Dabei gibt es sooooo viele...
---

Einzelne Programme bekommen hier ja einen separaten Artikel - sofern ich von dem jeweiligen Softwareprodukt nicht nur 100% überzeugt bin, sondern es auch selber einsetze. 

Die anderen 99.9% interessanter Software könnte ich jetzt in einer willkürlichen Liste als weitere Alternativen auflisten. Und wenn das nicht schon andere für mich gemacht hätten, wäre das sogar sehr sinnvoll. Praktikabler (und sinnvoller!)  erscheint es mir aber, hier nur eine Top10-Liste solcher Listen zu hinterlegen. Eine "LoL"  <small>(List-of-Lists)</small> also. Leider überwiegend in englischer Sprache:
 

* <https://www.privacytools.io>  --  deren Bereich "Software" *beginnt* mit folgenden Gruppen: "Email Clients", "Email Alternatives", "Instant Messenger", "Video & Voice Messenger", "Password Manager",  "Calendar and Contacts Sync", "File Encryption", "File Sharing", "Secure File Sync" 
* <https://alternativeto.net> -- man gibt den Namen einer zu ersetzenden Software ein und bekommt eine Liste von Alternativen als Resultat
* <https://prism-break.org/de/>  --  "Opt out of global data surveillance programs like PRISM, XKeyscore and Tempora."  - die Seite selber ist auf Deutsch
* <https://wiki.debian.org/FreedomBox/LeavingTheCloud> -- die Dokumentation der FreedomBox umfasst auch eine Liste etlicher Alternativen, die nicht zwingend in FreedomBox selber enthalten sind 
* <https://github.com/Kickball/awesome-selfhosted>  --  hunderte Projekte für einen eigenen Server
* <https://twofactorauth.org>  --  listet die Projekte, die Zweifaktor-Authentisierung unterstützen

<small>Update 03.2020:</small>

* <https://opensource.builders/>  --  "On a mission to find great open-source software", listet etliche freie Software zum selber hosten

