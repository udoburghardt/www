---
title: Bitwarden
date: 2019-08-20T09:40:02+02:00
draft: false
description: bitwarden.com implementiert eine Alternative zu online-Passwort-Managern wie LastPass. Insbesondere ist auch eine "self-hosted" Variante verfügbar 
category: Review
tags: [crypto, sicherheit, verschlüsselung, passwortmanager, self-hosted] 
---

Passwort-Manager gibt es wohl Dutzende. Und zwar sowohl freie (Open-Source) als auch kommerzielle Produkte. 

Bitwarden realisiert seine Fähigkeiten mit offener Software, die Quelltexte sind frei zugänglich auf Github verfügbar. Neben der Nutzung deren Server gibt es auch eine "self-hosted" Variante, die mit Hilfe von Docker alle Bestandteile auf einem eigenen Server realisiert. Damit bleiben die Daten "zu Hause" bei mir, statt im Ausland gespeichert zu werden.

Mittlerweile gibt es sogar weitere Implementierungen von unabhängigen Programmierern. Man ist also nicht einmal auf Bitwarden ('the Company') angewiesen. 


Highlights
----------
* *self-hosted* - wenn man das will. Auch in der kostenfreien Version. Die Daten liegen somit auf dem eigenen Server und nirgends anders :-)
* stand-alone Anmwendungen für Linux/Mac/Windows
* Plug-Ins für alle Brauser
* Android- und iOS-Applikation verfügbar


Nachteile
---------
* um *alle* möglichen Funktionen nutzen zu können, braucht man eine Lizenz. Als Einzelnutzer möchte man vielleicht für 10 US-Taler pro Jahr die "Premium"-Version haben. Damit unterstütze ich den Programmierer, auch wenn ich die zusätzlichen Funktionen eigentlich gar nicht nutze. Familien bekommen für 12 US-Taler pro Jahr das Paket "Family", das aber zunächst keine "Premium"-Funktionen enthält. Außerdem gibt es noch "Team und "Enterprise" für die geschäftliche Nutzung. Die Kombinationsmöglichkeiten sind unübersichtlich. Ein einzelner Nutzer ist aber mit der kostenfreien Version sehr gut bedient.


Nutzung
----------

**Desktop** = separate Anwendung für Linux/Mac/Windows. Wenn man sich _nur_ dies installiert muss man beim Anmelden im Webbrowser (der ja dann KEINE Passworte mehr gespeichert haben soll):

* die Login-Seite der Webseite aufrufen
* im Bitwarden (mit sehr komfortabler Volltextsuche) die Logindaten suchen
* den Nutzernamen kopieren und im Browser in das passende Feld einfügen
* das Passwort im Bitwarden kopieren und im Browser in das passende Feld einfügen

Das ist ein ziemlicher Komfortverlust; das ist schlicht umständlich. Aber das ist die sicherste Methode - wenn man mit nur *einem* Computer arbeitet und nur ein einziges Betriebssystem "an" hat. (Der nächste "Paranoia-Level" nutzt so etwas wie QubesOS...)

**Browser-Addon** = integral im Browser installiert, es gibt kein separates, eigenständiges Programm.

* man muss einmalig ein Masterpasswort zum Entriegeln eingeben. Dieses Passwort sollte stark sein. Und es sollte nach kurzer Zeit "vergessen" werden. Ob nach 5 Minuten oder erst nach drei Stunden ist natürlich einstellbar

* das Plugin füllt dann vollautomatisch Name/Passwort-Felder aus, was extrem bequem ist

Das ist aber NICHT die von mir genutzte und empfohlene Variante, ich mache das geringfügig anders:

* ich rufe die Login-Seite einer Webseite auf. Oben rechts befindet sich das Symbol von Bitwarden. Ein Klick öffnet eine kurze (!) Dropdown-Liste mit möglichen Name/Passwort-Paaren. Ein letzter Klick füllt Name/Passwort in die Felder. <br />Ich vermeide also das vollautomatische Ausfüllen. Damit hoffe ich zu verhindern, dass eine gekaperte Webseite die bereits ausgefüllten Name/Passwort-Felder abfragt...


**Notbetrieb**: der Vollständigkeit halber sei erwähnt, dass es auch einen Webzugriff auf den Inhalt des "Vaults" gibt --> https://vault.bitwarden.com/#/

* Alle Varianten sind gleichzeitig nutzbar! Die Synchronisation geschieht dabei unauffällig im Hintergrund

**Zweiter Faktor**: ich habe den _erstmaligen_ Zugang zum Vault mit einem zweiten Faktor gesichert. Und zwar mit dem "Google-Authenticator", der eigentlich gar nichts mit Google zu tun hat. Die Implementierung der 6-stelligen Zufallszahlen, die alle 30 Sekunden wechseln, ist genormt und es gibt darum etliche kompatible(!) Implementierungen. 

Für Windows nutze ich beispielsweise https://github.com/winauth/winauth.

Ich kann *dieselben* Codes aber auch auf der Kommandozeile abfragen oder auf das Telefon oder mein Tablett schauen - immer bekomme ich dieselbe Anzeige für "Zweiter Faktor für Bitwarden ist nun 123456".

Ein Brute-Force-Angriff auf den Webzugriff ist somit massiv erschwert weil

  * ich ein gutes Passwort verwende (nicht abschwächen!)
  * sich der zweite Teil eben alle 30 Sekunden verändert

**Audit**: Bitwarden hat die Software letztes Jahr von einer externen Firma untersuchen lassen. Die Ergebnisse sind öffentlich:
https://blog.bitwarden.com/bitwarden-completes-third-party-security-audit-c1cc81b6d33   --  natürlich in englischer Sprache...

**Notizen**: zu guter Letzt kann man natürlich auch andere Dinge ("Notes") dort abspeichern, beispielsweise Kontonummern oder wichtige Telefonnummern oder PINs der eigenen Telefon-SIMs oder ...




Links
-----
* <https://bitwarden.com> 
* <https://help.bitwarden.com/article/install-on-premise/>  --  Installation auf eigenen Server
* <https://github.com/bitwarden/>  --  frei verfügbare Quelltexte

